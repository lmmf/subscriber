package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// JSON payload we take as input. We decode into this struct
type subscribingUser struct {
	Name, Email string
}

// The message we write back upon successful subscription
type subcribedMessage struct {
	Message string
}

// struct which holds state, we take the HTTP request and put the user to
//subscribe into the channel. This allows us to push all of the "subscription
//effort" (writing to disk) onto a single worker we start
type subscriberW struct {

	// The JSON payloads of requests go into here
	scribers chan subscribingUser
}

// Main interface we use to share the channel across the handler and
type subscriber interface {

	// Passed into the http listen call.
	subscribe(rsp http.ResponseWriter, req *http.Request)
	// we invoke this as a go routine with the user, puts things on the scribers
	// channel
	subscribeUser(user subscribingUser)

	// Ran once on startup, pulls things off the scribers channel
	handleScribers()

	ServeHTTP(rsp http.ResponseWriter, r *http.Request)
}

func (s subscriberW) ServeHTTP(rsp http.ResponseWriter, r *http.Request) {
	fmt.Printf("Handling: %s, %s\n", r.Method, r.URL.EscapedPath())
	switch r.URL.EscapedPath() {
	case "/subscribe":
		s.subscribe(rsp, r)
	default:
		rsp.WriteHeader(404)
	}
}

func storeUser(user subscribingUser) {
	f, err := os.OpenFile("./subs/subscribers.json",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	toWrite, err := json.Marshal(user)
	if err != nil {
		fmt.Println(err)
	}
	_, _ = f.Write(toWrite)
	_, _ = f.WriteString("\n")

}

func (s *subscriberW) handleScribers() {
	for {
		subscriber := <-s.scribers
		fmt.Printf("Storing %+v", subscriber)
		storeUser(subscriber)
	}
}

func (s *subscriberW) subscribeUser(user subscribingUser) {
	s.scribers <- user
}

func (s subscriberW) subscribe(rsp http.ResponseWriter, req *http.Request) {
	fmt.Printf("Handling: %s, %s\n", req.Method, req.URL.EscapedPath())
	switch req.Method {
	case "POST":
		defer req.Body.Close()
		user := subscribingUser{}
		json.NewDecoder(req.Body).Decode(&user)
		go s.subscribeUser(user)
		message := subcribedMessage{}
		message.Message = fmt.Sprintf("Thanks for subscribing %s", user.Name)

		json.NewEncoder(rsp).Encode(message)
	default:
		rsp.WriteHeader(404)
	}
}

func waitForShutdown(srv *http.Server) {
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// Block until we receive our signal.
	<-interruptChan

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	srv.Shutdown(ctx)

	log.Println("Shutting down")
	os.Exit(0)
}

func main() {
	s := subscriberW{scribers: make(chan subscribingUser, 10)}
	go s.handleScribers()
	srv := &http.Server{
		Handler:      s,
		Addr:         ":8080",
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		log.Println("Starting Server")
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	// Graceful Shutdown
	waitForShutdown(srv)
}
