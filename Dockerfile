FROM golang:alpine as builder
WORKDIR $GOPATH/src/github.com/leofarrell/subscriber
copy . .

RUN \
	apk add --no-cache curl git \
	&& curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh \
	&& dep ensure -vendor-only \
	&& CGO_ENABLED=0 GOOS=linux go build \
		-a -installsuffix cgo \
		-ldflags "-X main.Version=$(cat VERSION)" \
		-o subscriber \
		.

FROM alpine
RUN apk add --no-cache ca-certificates
COPY --from=builder /go/src/github.com/leofarrell/subscriber/subscriber /bin/subscriber

EXPOSE 8080

CMD ["/bin/subscriber"]
