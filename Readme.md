A basic go project.

takes a POST, writes it out to a file.

Should safely access the file one request at a time, but will thrash the FS (Constantly opens and closes files). We open/close the file on every attempt because I don't really want to think about buffering the output to disk yet.
